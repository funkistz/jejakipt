<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faculty;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faculty = Faculty::all();

        $data = [
          'faculty' => $faculty
        ];

        return view('faculty.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
          'form_action' => route('faculty.store'),
        ];

        return view('faculty.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Faculty::create( $request->except('_token') );

        \Session::flash('alert-success', 'Fakulti berjaya ditambah.');
        return redirect()->route('faculty.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $faculty = Faculty::find($id);

        $data = [
          'faculty' => $faculty,
        ];

        return view('faculty.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
          'form_action' => route('faculty.update', $id),
          'method' => 'PUT',
          'faculty' => Faculty::find($id)
        ];

        return view('faculty.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Faculty::find($id)->update( $request->except('_token') );

        \Session::flash('alert-success', 'Fakulti berjaya dikemas kini.');
        return redirect()->route('faculty.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Faculty::find($id)->delete();
    }

    public function getJson(Request $request)
    {
        $faculty = new Faculty;

        $faculty = $faculty->get();

        return $faculty->toJson();
    }

    public function getJsonOne($id)
    {
        $faculty = Faculty::where('id', $id)->first();

        return $faculty->toJson();
    }
}
