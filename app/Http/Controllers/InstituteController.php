<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Institute;
use App\Models\News;
use App\Models\InstituteProgram;
use App\Models\Faculty;
use App\Models\Program;
use Illuminate\Support\Facades\DB;

class InstituteController extends Controller
{

    public $states = [
      '1' => 'Johor',
      '2' => 'Kedah',
      '3' => 'Kelantan',
      '4' => 'Terengganu',
      '5' => 'Melaka',
      '6' => 'Negeri Sembilan',
      '7' => 'Pahang',
      '8' => 'Perak',
      '9' => 'Perlis',
      '10' => 'Pulau Pinang',
      '11' => 'Sabah',
      '12' => 'Sarawak',
      '13' => 'Selangor',
      '14' => 'W.P. Kuala Lumpur',
      '15' => 'W.P. Labuan',
      '16' => 'W.P. Putrajaya',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $institute = Institute::all();

        $data = [
          'institute' => $institute
        ];

        return view('institute.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
          'form_action' => route('institute.store'),
          'states' => $this->states
        ];

        return view('institute.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Institute::create( $request->except('_token') );

        \Session::flash('alert-success', 'Institut berjaya ditambah.');
        return redirect()->route('institute.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $institute = Institute::find($id);

        $data = [
          'institute' => $institute,
          'news' => News::where('institute_id', $id)->get(),
          'instituteProgram' => InstituteProgram::with(['faculty', 'program'])->where('institute_id', $id)->get(),
          'faculty' => Faculty::all(),
          'program' => Program::all(),
        ];

        return view('institute.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
          'form_action' => route('institute.update', $id),
          'method' => 'PUT',
          'states' => $this->states,
          'institute' => Institute::find($id)
        ];

        return view('institute.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Institute::find($id)->update( $request->except('_token') );

        \Session::flash('alert-success', 'Institut berjaya dikemas kini.');
        return redirect()->route('institute.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Institute::find($id)->delete();
    }

    public function getJson(Request $request)
    {
        $search = $request->get('search');
        $type = $request->get('type');
        $state = $request->get('state_name');
        $program = $request->get('program_code');
        $faculty = $request->get('faculty_code');
        $level = $request->get('level');

        $institute = new Institute;

        if(!empty($search)){
          $institute = $institute->where(function($query) use($search)
          {
              $query->where('name', 'LIKE', '%' . $search . '%')
              ->orWhere('city', 'LIKE', '%' . $search . '%')
              ->orWhere('state_name', 'LIKE', '%' . $search . '%');
          });
        }

        if(!empty($type)){
            $institute = $institute->where('type', $type);
        }

        if(!empty($state)){
          $state = strtolower($state);
          if($state != 'all'){
            $institute = $institute->where('state_name', $state);
          }
        }

        if(!empty($program)){
          $institute = $institute->whereHas('institutePrograms', function($query) use($program){
            $query->where('program_code', $program);
          });
        }

        if(!empty($faculty)){
          $institute = $institute->whereHas('institutePrograms', function($query) use($faculty){
            $query->where('faculty_code', $faculty);
          });
        }

        if(!empty($level)){
          $institute = $institute->whereHas('institutePrograms', function($query) use($level){
            $query->where('level', $level);
          });
        }

        $institute = $institute->get();

        return $institute->toJson();
    }

    public function getJsonOne($id)
    {
        $institute = Institute::where('id', $id)->with(['news', 'institutePrograms', 'institutePrograms.program', 'institutePrograms.faculty'])->first();

        $institute->news = $institute->news->map(function($item, $key){
            $item->date = $item->created_at->diffForHumans();
            return $item;
        });

        $institute->institutePrograms = $institute->institutePrograms->map(function($item, $key){
            $item->duration = $item->durationReal;
            return $item;
        });

        return $institute->toJson();
    }

    public function getStates()
    {
        $institute = Institute::groupBy('state_name')->select('state_name', DB::raw('count(*) as total'))->get();

        return $institute->toJson();
    }

    public function getCategories()
    {
        $institute = Institute::groupBy('type')->select('type', DB::raw('count(*) as total'))->get();

        return $institute->toJson();
    }

    public function getRankings($type)
    {
        if($type == 'world'){
          $institute = Institute::where('ranking_world_no', '!=', 'NULL')->orderBy('ranking_world_no', 'asc')->get();
        }else{
          $institute = Institute::where('ranking_asia_no', '!=', 'NULL')->orderBy('ranking_asia_no', 'asc')->get();
        }

        $institute = $institute->map(function($item, $key){
            if (strpos($item->name, '-') !== false) {
                $item->name = substr($item->name, 0, strrpos( $item->name, '-'));;
            }
            return $item;
        });
        //
        // $institute = $institute->sortBy("ranking_world_no")->unique('group')->values()->all();
        //
        //
        if($type == 'world'){
          $institute = $institute->unique('group')->values()->all();
        }else{
          $institute = $institute->unique('group')->values()->all();
        }

        return json_encode($institute);
    }
}
