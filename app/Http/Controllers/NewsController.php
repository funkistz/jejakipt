<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Institute;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();

        $data = [
          'news' => $news
        ];

        return view('news.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
          'form_action' => route('news.store'),
          'institute' => Institute::all()
        ];

        return view('news.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News();
        $data = $news->data;
        $data['title'] = $request->title;
        $data['content'] = $request->content;
        $news->data = $data;
        $news->institute_id = $request->institute_id;

        $news->save();

        \Session::flash('alert-success', 'Info berjaya ditambah.');
        return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::find($id);

        $data = [
          'news' => $news,
        ];

        return view('news.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
          'form_action' => route('news.update', $id),
          'institute' => Institute::all(),
          'method' => 'PUT',
          'news' => News::find($id)
        ];

        return view('news.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::find($id);
        $data = $news->data;
        $data['title'] = $request->title;
        $data['content'] = $request->content;
        $news->data = $data;
        $news->institute_id = $request->institute_id;

        $news->save();

        \Session::flash('alert-success', 'Info berjaya dikemas kini.');
        return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::find($id)->delete();
    }

    public function getJson(Request $request)
    {
        $news = new News;

        $news = $news->with(['institute'])->get()->map(function($item, $key){
            $item->date = $item->created_at->diffForHumans();
            return $item;
        });

        return json_encode($news);
    }

    public function getJsonOne($id)
    {
        $news = News::where('id', $id)->first();

        $news->date = $news->created_at->diffForHumans();

        return $news->toJson();
    }
}
