<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Program;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $program = Program::all();

        $data = [
          'program' => $program
        ];

        return view('program.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
          'form_action' => route('program.store'),
        ];

        return view('program.form')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Program::create( $request->except('_token') );

        \Session::flash('alert-success', 'Program berjaya ditambah.');
        return redirect()->route('program.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $program = Program::find($id);

        $data = [
          'program' => $program,
        ];

        return view('program.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = [
          'form_action' => route('program.update', $id),
          'method' => 'PUT',
          'program' => Program::find($id)
        ];

        return view('program.form')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Program::find($id)->update( $request->except('_token') );

        \Session::flash('alert-success', 'Program berjaya dikemas kini.');
        return redirect()->route('program.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Program::find($id)->delete();
    }

    public function getJson(Request $request)
    {
        $program = new Program;

        $program = $program->get();

        return $program->toJson();
    }

    public function getJsonOne($id)
    {
        $program = Program::where('id', $id)->first();

        return $program->toJson();
    }
}
