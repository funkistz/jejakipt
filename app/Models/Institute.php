<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'type',
        'name',
        'address',
        'city',
        'postcode',
        'state_id',
        'lat',
        'lng',
        'email',
        'phone',
        'faks',
        'website'
    ];

    protected $dates = [
        'created_at',
        'deleted_at'
    ];

    public function institutePrograms()
    {
        return $this->hasMany('App\Models\InstituteProgram');
    }

    public function news()
    {
        return $this->hasMany('App\Models\News');
    }

    public function getTypeNameAttribute()
    {
        if($this->attributes['type'] == 1){
          return 'IPTA';
        }else if($this->attributes['type'] == 2){
          return 'IPTS';
        }else if($this->attributes['type'] == 3){
          return 'Kolej Komuniti';
        }else if($this->attributes['type'] == 4){
          return 'Poli';
        }

        return '';
    }
}
