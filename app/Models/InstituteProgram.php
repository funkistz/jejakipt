<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class InstituteProgram extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'institute_id',
        'program_code',
        'faculty_code',
        'level',
        'data',
        'major'
    ];

    protected $dates = [
        'created_at',
        'deleted_at'
    ];

    protected $casts = [
        'data' => 'json',
    ];

    public function institute()
    {
        return $this->belongsTo('App\Models\Institute');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\Program', 'program_code', 'code');
    }

    public function faculty()
    {
        return $this->belongsTo('App\Models\Faculty', 'faculty_code', 'code');
    }

    public function getDurationRealAttribute()
    {
        if($this->attributes['duration_type'] == 1){
          return $this->attributes['duration'] . ' months';
        }else if($this->attributes['duration_type'] == 2){
          return $this->attributes['duration'] . ' sem';
        }
    }

    public function getLevelNameAttribute()
    {
        if($this->attributes['level'] == 'b'){
          return 'Degree';
        }else if($this->attributes['level'] == 'd'){
          return 'Diploma';
        }else if($this->attributes['level'] == 'm'){
          return 'Master';
        }else if($this->attributes['level'] == 'f'){
          return 'Foundation';
        }else if($this->attributes['level'] == 's'){
          return 'Sijil';
        }

        return '';
    }
}
