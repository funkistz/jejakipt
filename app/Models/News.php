<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'institute_id',
        'data',
    ];

    protected $dates = [
        'deleted_at',
        'created_at'
    ];

    protected $casts = [
        'data' => 'json',
    ];

    public function institute()
    {
        return $this->belongsTo('App\Models\Institute');
    }

}
