toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};

function toast(type, msg) {
    toastr.options = {
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "showDuration": "5000",
        "hideDuration": "5000",
        "timeOut": "5000",
        "preventDuplicates": true,
    };

    switch (type) {
        case 'success':
            toastr.success(msg);
            break;
        case 'info':
            toastr.info(msg);
            break;
        case 'warning':
            toastr.warning(msg);
            break;
        case 'danger':
        case 'error':
            toastr.error(msg);
            break;
    }
}
