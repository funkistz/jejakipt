@extends('layouts.app')

@section('template_title')
  Dashboard
@endsection

@section('content')

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

      <div class="m-portlet m-portlet--tab">
  			<div class="m-portlet__head">
  				<div class="m-portlet__head-caption">
  					<div class="m-portlet__head-title">
  						<h3 class="m-portlet__head-text">Numbers of IPT based on Category</h3>
  					</div>
  				</div>
  			</div>
        <canvas id="myChart" width="400" height="400"></canvas>
  		</div>
      
@endsection

@push('js')
<script>

var ctx = document.getElementById("myChart");
var myChart = new Chart(ctx, {
  type: 'doughnut',
  data: {
      labels: ["IPTA", "IPTS", "Kolej Komuniti", "Politeknik"],
      datasets: [{
          label: '# of Votes',
          data: [
            {{ $instituteCategories[0]['total'] }},
            {{ $instituteCategories[1]['total'] }},
            {{ $instituteCategories[2]['total'] }},
            {{ $instituteCategories[3]['total'] }}
          ],
          backgroundColor: [
              '#2962FF',
              '#FFD600',
              '#00C853',
              '#D50000'
          ],
      }]
  },
  datalabels: {
    anchor: 'center'
  },
  options : {
    maintainAspectRatio : false,
    pieceLabel: {
      render: 'label',
      fontColor: '#000',
      position: 'outside'
    },
  }
});

</script>
@endpush
