@extends('layouts.app')

@section('template_title')
  Fakulti Baru
@endsection

@section('content')

<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
              @if(isset($method))
              Kemas Kini Fakulti
              @else
              Fakulti Baru
              @endif
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" action="{{ $form_action }}" method="post" >

        @if(isset($method))
        {{ method_field($method) }}
        @endif
        {{ csrf_field() }}

				<div class="m-portlet__body">
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Nama</label>
            <div class="col-lg-6">
			        <input name="name" type="text" class="form-control m-input m-input--solid" placeholder="Nama fakulti" value="{{ $faculty->name or old('name') }}" required>
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Deskripsi</label>
            <div class="col-lg-6">
              <textarea name="description" class="form-control m-input m-input--solid" rows="3" placeholder="Deskripsi fakulti" value="{{ $faculty->description or old('description') }}"></textarea>
            </div>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="submit" class="btn btn-success">Submit</button>
						<a class="btn btn-secondary" href="{{ route('faculty.index') }}">Cancel</a>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>

@endsection
