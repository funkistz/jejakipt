@extends('layouts.app')

@section('template_title')
  Institut Baru
@endsection

@section('content')

<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
              @if(isset($method))
              Kemas Kini Institut
              @else
              Institut Baru
              @endif
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" action="{{ $form_action }}" method="post" >

        @if(isset($method))
        {{ method_field($method) }}
        @endif
        {{ csrf_field() }}

				<div class="m-portlet__body">
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Jenis</label>
            <div class="col-lg-6">
              <select name="type" class="form-control m-input m-input--solid" value="{{ $institute->type or old('type') }}">
                <option value="1">IPTA</option>
                <option value="2">IPTS</option>
                <option value="3">Kolej Komuniti</option>
                <option value="4">Politeknik</option>
  						</select>
            </div>
					</div>
					<div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Kod *</label>
            <div class="col-lg-6">
						  <input name="code" type="text" class="form-control m-input m-input--solid" placeholder="Kod institut" value="{{ $institute->code or old('code') }}" required>
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Nama *</label>
            <div class="col-lg-6">
			        <input name="name" type="text" class="form-control m-input m-input--solid" placeholder="Nama institut" value="{{ $institute->name or old('name') }}" required>
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Emel</label>
            <div class="col-lg-6">
			        <input name="email" type="email" class="form-control m-input m-input--solid" placeholder="Emel institut" value="{{ $institute->email or old('email') }}">
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Telefon</label>
            <div class="col-lg-6">
			        <input name="phone" type="text" class="form-control m-input m-input--solid" placeholder="No. Telefon institut" value="{{ $institute->phone or old('phone') }}">
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Faks</label>
            <div class="col-lg-6">
			        <input name="faks" type="text" class="form-control m-input m-input--solid" placeholder="No. Faks institut" value="{{ $institute->faks or old('faks') }}">
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Laman sesawang</label>
            <div class="col-lg-6">
			        <input name="website" type="text" class="form-control m-input m-input--solid" placeholder="Laman sesawang institut" value="{{ $institute->website or old('website') }}">
            </div>
					</div>
          <div class="form-group m-form__group row">
            <label class="col-lg-2 col-form-label">Latitud dan longitud *</label>
            <div class="col-lg-6">
              <div class="input-group">
                <input name="lat" type="number" step="0.000001" class="form-control m-input m-input--solid" placeholder="Latitud" required value="{{ $institute->lat or old('lat') }}">
                <input name="lng" type="number" step="0.000001" class="form-control m-input m-input--solid" placeholder="Longitud" required value="{{ $institute->lng or old('lng') }}">
              </div>
            </div>
          </div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Alamat</label>
            <div class="col-lg-6">
              <textarea name="address" class="form-control m-input m-input--solid" rows="3" placeholder="Alamat penuh institut" value="{{ $institute->address or old('address') }}"></textarea>
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Poskod</label>
            <div class="col-lg-6">
              <input name="postcode" type="number" class="form-control m-input m-input--solid" placeholder="Poskod institut" value="{{ $institute->postcode or old('postcode') }}">
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Bandar</label>
            <div class="col-lg-6">
              <input name="city" type="text" class="form-control m-input m-input--solid" placeholder="Bandar Institut" value="{{ $institute->city or old('city') }}">
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Negeri</label>
            <div class="col-lg-6">
              <select name="state_id" class="form-control m-input m-input--solid" value="{{ $institute->state_id or old('state_id') }}">
                @foreach($states as $key => $state)
  							<option value="{{ $key }}">{{ $state }}</option>
                @endforeach
  						</select>
            </div>
					</div>


				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="submit" class="btn btn-success">Submit</button>
						<a class="btn btn-secondary" href="{{ route('institute.index') }}">Cancel</a>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>

@endsection

@push('js')
<script>

  $('[name="type"]').val('{{ $institute->type or null }}');
  $('[name="state_id"]').val('{{ $institute->state_id or null }}');

</script>
@endpush
