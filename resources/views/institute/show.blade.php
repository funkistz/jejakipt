@extends('layouts.app')

@section('template_title')
  Maklumat Institut
@endsection

@section('content')
<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<span class="m-portlet__head-icon m--hide">
					<i class="flaticon-statistics"></i>
				</span>
				<h3 class="m-portlet__head-text">
					Maklumat
				</h3>
				<h2 class="m-portlet__head-label m-portlet__head-label--danger">
					<span>{{ $institute->name }}</span>
				</h2>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('institute.edit', $institute->id) }}" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-cog"></i></a>
				</li>
			</ul>
		</div>
	</div>
  <div class="m-portlet__body">
    <div class="table-responsive">
		    <table class="table">
				<tbody>
					<tr>
					 	<th width="200">Kod</th>
					 	<td>{{ $institute->code }}</td>
					</tr>
					<tr>
					 	<th>Nama</th>
					 	<td>{{ $institute->name }}</td>
					</tr>
					<tr>
					 	<th>Emel</th>
					 	<td>{{ $institute->email }}</td>
					</tr>
					<tr>
					 	<th>No. Telefon</th>
					 	<td>{{ $institute->phone }}</td>
					</tr>
					<tr>
					 	<th>Faks</th>
					 	<td>{{ $institute->faks }}</td>
					</tr>
					<tr>
					 	<th>Laman Sesawang</th>
					 	<td>{{ $institute->website }}</td>
					</tr>
					<tr>
					 	<th>Latitud dan Longitud</th>
					 	<td>{{ $institute->lat }} - {{ $institute->lng }}</td>
					</tr>
					<tr>
					 	<th>Alamat</th>
					 	<td>{{ $institute->address }}</td>
					</tr>
					<tr>
					 	<th>Poskod</th>
					 	<td>{{ $institute->postcode }}</td>
					</tr>
					<tr>
					 	<th>Bandar</th>
					 	<td>{{ $institute->city }}</td>
					</tr>
					<tr>
					 	<th>Negeri</th>
					 	<td>{{ $institute->state }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!--begin::Portlet-->
<div class="m-portlet m-portlet--tabs">
    <div class="m-portlet__head">
        <div class="m-portlet__head-tools">
            <ul class="nav nav-tabs m-tabs-line m-tabs-line--success m-tabs-line--2x" role="tablist">
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#tab_program" role="tab">
                        <i class="fa fa-graduation-cap"></i> Program
                    </a>
                </li>
                <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#tab_news" role="tab">
                        <i class="fa fa-newspaper-o"></i> Info dan Aktiviti
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_program" role="tabpanel">

              <h5 class="mb-4">Tambah Program Baru</h5>
              <form class="m-form m-form--fit m-form--label-align-right" action="{{ route('institute-program.store') }}" method="post">
                {{ csrf_field() }}

                <input name="institute_id" type="hidden" value="{{ $institute->id }}" />
                <div class="form-group m-form__group row">
      						<label class="col-lg-2 col-form-label">Fakulti</label>
                  <div class="col-lg-6">
                    <select name="faculty_code" class="form-control m-input m-input--solid mr-4">
                      @foreach($faculty as $fac)
                      <option value="{{ $fac->code }}">{{ $fac->name }}</option>
                      @endforeach
        						</select>
                  </div>
      					</div>
                <div class="form-group m-form__group row">
      						<label class="col-lg-2 col-form-label">Program</label>
                  <div class="col-lg-6">
                    <select name="program_code" class="form-control m-input m-input--solid mr-4" >
                      @foreach($program as $prog)
                      <option value="{{ $prog->code }}">{{ $prog->name }}</option>
                      @endforeach
        						</select>
                  </div>
      					</div>
                <div class="form-group m-form__group row">
      						<label class="col-lg-2 col-form-label">Level</label>
                  <div class="col-lg-6">
                    <select name="level" class="form-control m-input m-input--solid mr-4">
                      <option value="b">Bachelor</option>
                      <option value="d">Diploma</option>
                      <option value="m">Master</option>
                      <option value="p">PHD</option>
                      <option value="f">Foundation</option>
                      <option value="s">Sijil</option>
        						</select>
                  </div>
      					</div>
                <div class="form-group m-form__group row">
      						<label class="col-lg-2 col-form-label">Major</label>
                  <div class="col-lg-6">
                    <input name="major" type="text" class="form-control m-input m-input--solid" placeholder="Major">
                  </div>
      					</div>

                <div class="m-portlet__foot m-portlet__foot--fit">
        					<div class="m-form__actions">
        						<button type="submit" class="btn btn-success">Tambah</button>
        					</div>
        				</div>
              </form>

              <table width="100%" class="table mt-4">
                <thead>
                  <tr>
                    <th>Fakulti</th>
                    <th>Program</th>
                    <th>Level</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($instituteProgram as $program)
                  <tr>
                    <td>{{ $program->faculty->name }}</td>
                    <td>{{ $program->program->name }}</td>
                    <td>{{ $program->levelName }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>

            </div>
            <div class="tab-pane" id="tab_news" role="tabpanel">

              <table width="100%" class="table">
                <thead>
                  <tr>
                    <th>Tajuk</th>
                    <th>Kandungan</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($news as $new)
                  <tr>
                    <td>
                      {{ $new->data['title'] }}
                    </td>
                    <td>
                      {{ $new->data['content'] }}
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>

            </div>
        </div>
    </div>
</div>
<!--end::Portlet-->

@endsection

@push('js')
<script>

  $('[name="level"]').val('');
  $('[name="program_code"]').val('');
  $('[name="faculty_code"]').val('');

</script>
@endpush
