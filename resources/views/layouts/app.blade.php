<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--begin::Web font -->
    		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    		<script>
              WebFont.load({
                google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
                active: function() {
                    sessionStorage.fonts = true;
                }
              });
    		</script>
    		<!--end::Web font -->

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@if (trim($__env->yieldContent('template_title')))@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
        <meta name="description" content="">
        <meta name="author" content="Syimir">

        {{-- Fonts --}}
        @yield('template_linked_fonts')

        {{-- Styles --}}
        <link href="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
    		<!--end::Page Vendors -->
    		<link href="{{ asset('assets/vendors/base/vendors.bundle.css')}}" rel="stylesheet" type="text/css" />
    		<link href="{{ asset('assets/demo/default/base/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    		<!--end::Base Styles -->
    		<link rel="shortcut icon" href="{{ asset('assets/demo/default/media/img/logo/favicon.ico') }}" />

        @stack('css')

    </head>
    <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >

      	<div class="m-grid m-grid--hor m-grid--root m-page">

        @include('partials.nav')

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

          @include('partials.side-navigation')

          <div class="m-grid__item m-grid__item--fluid m-wrapper">
  					<!-- BEGIN: Subheader -->
  					<div class="m-subheader ">
  						<div class="d-flex align-items-center">
  							<div class="mr-auto">
  								<h3 class="m-subheader__title ">
  									@yield('template_title')
  								</h3>
  							</div>
  						</div>
  					</div>
  					<!-- END: Subheader -->
  					<div class="m-content">
              @yield('content')
  					</div>
  				</div>

        </div>

        @include('partials.footer')

        </div>

        <script src="{{ asset('assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
    		<script src="{{ asset('assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
    		<script src="{{ asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>
    		<script src="{{ asset('assets/app/js/dashboard.js')}}" type="text/javascript"></script>

        <script src="{{ asset('js/jejakipt.js') }}" type="text/javascript"></script>
        <script>

        @foreach (['danger', 'error', 'warning', 'success', 'info'] as $type)
            @if(Session::has('alert-' . $type))
                toast('{{ $type }}', "{{ Session::get('alert-' . $type) }}");
            @endif
        @endforeach

        </script>

        @stack('js')
    </body>
</html>
