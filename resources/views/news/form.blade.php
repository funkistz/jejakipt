@extends('layouts.app')

@section('template_title')
  Info dan Aktiviti Baru
@endsection

@section('content')

<div class="m-portlet m-portlet--tab">
			<div class="m-portlet__head">
				<div class="m-portlet__head-caption">
					<div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
						<h3 class="m-portlet__head-text">
              @if(isset($method))
              Kemas Kini
              @else
              Butiran
              @endif
						</h3>
					</div>
				</div>
			</div>
			<!--begin::Form-->
			<form class="m-form m-form--fit m-form--label-align-right" action="{{ $form_action }}" method="post" >

        @if(isset($method))
        {{ method_field($method) }}
        @endif
        {{ csrf_field() }}

				<div class="m-portlet__body">
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Institut</label>
            <div class="col-lg-6">
              <select name="institute_id" class="form-control m-input m-input--solid" value="{{ $news->institute_id or old('institute_id') }}">
                @foreach($institute as $inst)
                <option value="{{ $inst->id }}">{{ $inst->name }}</option>
                @endforeach
  						</select>
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Title</label>
            <div class="col-lg-6">
			        <input name="title" type="text" class="form-control m-input m-input--solid" placeholder="Tajuk info atau aktiviti" value="{{ $news->data['title'] or old('title') }}" required>
            </div>
					</div>
          <div class="form-group m-form__group row">
						<label class="col-lg-2 col-form-label">Deskripsi</label>
            <div class="col-lg-6">
              <textarea name="content" class="form-control m-input m-input--solid" rows="3" placeholder="Deskripsi info atau aktiviti" value="{{ $news->data['content'] or old('content') }}"></textarea>
            </div>
					</div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions">
						<button type="submit" class="btn btn-success">Submit</button>
						<a class="btn btn-secondary" href="{{ route('news.index') }}">Cancel</a>
					</div>
				</div>
			</form>
			<!--end::Form-->
		</div>

@endsection

@push('js')
<script>

  $('[name="institute_id"]').val('{{ $news->institute_id or null }}');

</script>
@endpush
