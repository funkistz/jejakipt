@extends('layouts.app')

@section('template_title')
  Maklumat Institut
@endsection

@section('content')
<div class="m-portlet m-portlet--creative m-portlet--first m-portlet--bordered-semi">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<span class="m-portlet__head-icon m--hide">
					<i class="flaticon-statistics"></i>
				</span>
				<h3 class="m-portlet__head-text">
					Maklumat
				</h3>
				<h2 class="m-portlet__head-label m-portlet__head-label--danger">
					<span>{{ $institute->name }}</span>
				</h2>
			</div>
		</div>
		<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('institute.edit', $institute->id) }}" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-cog"></i></a>
				</li>
			</ul>
		</div>
	</div>
  <div class="m-portlet__body">
    <div class="table-responsive">
		    <table class="table">
				<tbody>
					<tr>
					 	<th width="200">Kod</th>
					 	<td>{{ $institute->code }}</td>
					</tr>
					<tr>
					 	<th>Nama</th>
					 	<td>{{ $institute->name }}</td>
					</tr>
					<tr>
					 	<th>Emel</th>
					 	<td>{{ $institute->email }}</td>
					</tr>
					<tr>
					 	<th>No. Telefon</th>
					 	<td>{{ $institute->phone }}</td>
					</tr>
					<tr>
					 	<th>Faks</th>
					 	<td>{{ $institute->faks }}</td>
					</tr>
					<tr>
					 	<th>Laman Sesawang</th>
					 	<td>{{ $institute->website }}</td>
					</tr>
					<tr>
					 	<th>Latitud dan Longitud</th>
					 	<td>{{ $institute->lat }} - {{ $institute->lng }}</td>
					</tr>
					<tr>
					 	<th>Alamat</th>
					 	<td>{{ $institute->address }}</td>
					</tr>
					<tr>
					 	<th>Poskod</th>
					 	<td>{{ $institute->postcode }}</td>
					</tr>
					<tr>
					 	<th>Bandar</th>
					 	<td>{{ $institute->city }}</td>
					</tr>
					<tr>
					 	<th>Negeri</th>
					 	<td>{{ $institute->state }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
