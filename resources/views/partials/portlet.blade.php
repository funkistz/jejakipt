<div class="m-portlet m-portlet--mobile">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					{{ $title }}
          @if(!empty($subtitle))
          <small>{{ $subtitle }}</small>
          @endif
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
    {{ $content }}
	</div>
</div>
