@extends('layouts.app')

@section('template_title')
  Program
@endsection

@section('content')

@component('partials.portlet')
    @slot('title')
        Senarai Program
    @endslot

    @slot('content')
    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
      <div class="row align-items-center">
        <div class="col-xl-8 order-2 order-xl-1">
          <div class="form-group m-form__group row align-items-center">
            <div class="col-md-4">
              <div class="m-input-icon m-input-icon--left">
                <input type="text" class="form-control m-input" placeholder="Search..." id="generalSearch">
                <span class="m-input-icon__icon m-input-icon__icon--left">
                  <span>
                    <i class="la la-search"></i>
                  </span>
                </span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
          <a href="{{ route('program.create') }}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
            <span>
              <i class="la la-plus"></i>
              <span>
                Tambah Program
              </span>
            </span>
          </a>
          <div class="m-separator m-separator--dashed d-xl-none"></div>
        </div>
      </div>
    </div>

    <table class="m-datatable" id="html_table" width="100%">
      <thead>
        <tr>
          <th>Nama</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        @foreach($program as $pro)
        <tr>
          <td>
            {{ $pro->name }}
          </td>
          <td>
            <a href="{{ route('program.edit', $pro->id) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="kemas kini">
              <i class="la la-edit"></i>
            </a>
            <button href="{{ route('program.destroy', $pro->id) }}" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill delete-sa2" title="Padam">
              <i class="la la-trash"></i>
            </button>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @endslot

@endcomponent



@endsection

@push('js')
<script>

var DatatableHtmlTableDemo = function() {
  //== Private functions

  // demo initializer
  var demo = function() {

    var datatable = $('.m-datatable').mDatatable({
      data: {
        saveState: {cookie: false},
      },
      search: {
        input: $('#generalSearch'),
      }
    });
  };

  return {
    //== Public functions
    init: function() {
      // init dmeo
      demo();
    },
  };
}();

jQuery(document).ready(function() {
  DatatableHtmlTableDemo.init();
});

jQuery(document).ready(function(){
  $(".m-bootstrap-select").selectpicker();

  $('.delete-sa2').click(function(e) {

      var url = $(this).attr('href');

      swal({
          title: 'Anda Pasti?',
          text: "Program akan padam!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Ya!',
          preConfirm: function(email) {
              return new Promise(function(resolve, reject) {
                  $.ajax({
                      type: "DELETE",
                      url: url,
                      headers: {
                          'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                      },
                      success: function(msg) {
                          resolve()
                      },
                      error: function(req, status, err) {
                          toast('error', 'Something when wrong');
                      }
                  });
              })
          },
      }).then(function(result) {
          if (result.value) {
              swal(
                  'Dipadam!',
                  'Program telah dipadam.',
                  'success'
              ).then(function(){
                location.reload();
              })
          }
      });
  });

});
</script>
@endpush
