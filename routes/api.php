<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/institute', 'InstituteController@getJson');
Route::get('/institute/categories', 'InstituteController@getCategories');
Route::get('/institute/rankings/{type}', 'InstituteController@getRankings');
Route::get('/institute/states', 'InstituteController@getStates');
Route::get('/institute/{id}', 'InstituteController@getJsonOne');

Route::get('/program', 'ProgramController@getJson');
Route::get('/program/{id}', 'ProgramController@getJsonOne');

Route::get('/faculty', 'FacultyController@getJson');
Route::get('/faculty/{id}', 'FacultyController@getJsonOne');

Route::get('/news', 'NewsController@getJson');
Route::get('/news/{id}', 'NewsController@getJsonOne');
